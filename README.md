# Spanish Checkers #

This is a project of an online game of the spanish variant of checkers or draughts. I've made this project as part of a university project as a proof of concept for me to learn NodeJS and Socket.io. It uses the following NodeJS libraries: Socket.io, Express, Body-parser.

It is by no means the best implementation of checkers, and it may lack some features, but it pretty much playable and the only thing that is required to play it is a relatively modern web browser with Html5 and javascript support, and of course, a friend to play with.

The game can be played online at [checkers.yaroslavps.com](http://checkers.yaroslavps.com).

## Лабораторная Работа №4, вариант 5: Испанские шашки ##

Выполнил Де Ла Пенья Смирнов Ярослав, группа K3200.